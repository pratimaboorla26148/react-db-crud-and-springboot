import React from 'react';

class DeleteEmp extends React.Component
{
    state = {
            id : "",
            status : ""
            }

    formIDChange = this.formIDChange.bind(this);
    handleSubmit = this.handleSubmit.bind(this);

    formIDChange(event)
    {
        this.setState({id : event.target.value })
//        console.log(this.state.id)
    }

    handleSubmit(event)
    {
        event.preventDefault();
//        console.log(this.state.id)
        const data = {
                    "id" : this.state.id
              }
      fetch('http://localhost:8080/emp/delete', {
              method : 'POST',
              body: JSON.stringify(data),
              headers: {'Content-Type': 'application/json'}
              })
              .then(response => response.json())
              .then(res => {
              this.setState({status : JSON.parse(res)})

                  })


    }

    render()
    {
        return (
            <div>
               <form onSubmit={this.handleSubmit} name="delete-form">
               <table>
                    <tbody>
                    <tr>
                    <td><label>ID : </label></td>
                    <td>
                    <input type="text" name="id" value={this.state.id} onChange={this.formIDChange}/>
                    </td></tr>
                    <tr><td colSpan="2" align="center">
                    <input type="submit" value="DELETE" />
                    </td></tr>
                    </tbody>
               </table>
               </form>
               <div>
                    {this.state.status === "" ?null
                    :this.state.status?
                    <h2>Employee with ID {this.state.id} is deleted</h2>
                    : <h2>There is no such Employee to Delete</h2>
                    }
               </div>
            </div>
            );
    }
}



export default DeleteEmp;