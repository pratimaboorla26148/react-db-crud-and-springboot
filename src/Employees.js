import React from 'react';
//import ReactDOM from 'react-dom';

class GetAllEmp extends React.Component
{

    state = {
             data: [],
             showResTable : false
            }
    handleSubmit = this.handleSubmit.bind(this);


    /*constructor(props){
        super(props);
      this.state = {
                  data: []
              }
         this.handleSubmit = this.handleSubmit.bind(this);
    }*/

    handleSubmit(event){
        event.preventDefault();

//        console.log('button clicked');
        fetch('http://localhost:8080/emp/getEmp')
            .then(response => response.json())
            .then(res => this.setState({ data : res }))
            .then(this.setState({ showResTable : true }))
    }

/*  componentDidMount() {
  console.log('Here here.....')

    fetch('http://localhost:8080/getEmp')
      .then(response => response.json())
      .then(res => this.setState({data : res}))
  }*/


    render()
    {
//     console.log(this.state.data)
        return (
        <div>
            <input type="button" value="Get All Employees" onClick={this.handleSubmit} ></input>
            <Result response={this.state.data} showTable = {this.state.showResTable}/>
        </div>
        )
    }
}

class Result extends React.Component
{
    /*constructor(props){
        super(props);
    }*/

    render(){
//        console.log(this.props.response);
//        alert(this.props.showTable);
        return (
            <div>
                <table>
                    { this.props.showTable ?
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                          </tr>
                      </thead>
                      :<thead></thead> }

                   <tbody>
               {this.props.response.map(function(emp,key){
                    return(
                           <tr key={key}>
                           <td>{emp.id}</td>
                           <td>{emp.name}</td></tr>
                    )})
               }
                    </tbody>
                </table>
            </div>
        )
    }
}

class Employees extends React.Component
{
/*constructor(props){
    super(props);
}*/
    render(){
        return (
            <div>
                <GetAllEmp />
            </div>
        );
    }
}



export default Employees;
//export {GetAllEmp,Result};


//ReactDOM.render(e(First),document.querySelector('#react'));
//ReactDOM.render(<First />,document.querySelector('#react'));
//ReactDOM.render(<Employees />,document.querySelector('#react'));


