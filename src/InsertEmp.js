import React from 'react';

class InsertEmp extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            id : "",
            name : "",
            status : ""
        }
        this.formIDChange = this.formIDChange.bind(this);
        this.formNameChange = this.formNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    formIDChange(event)
    {
        this.setState({id : event.target.value})
//        console.log(this.state.id)
    }

    formNameChange(event)
    {
        this.setState({name : event.target.value})
//        console.log(this.state.name)
    }


    handleSubmit(event)
    {
    event.preventDefault();
//        let formData = new FormData();
//        formData.append('id',this.state.id);
//        formData.append('name',this.state.name);

        const data = {
            "id" : this.state.id,
            "name" : this.state.name
        }
        fetch('http://localhost:8080/emp/insert', {
                method : 'POST',
                body: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
                })
                .then(response => response.json())
                .then(res => {
                this.setState({status : JSON.parse(res)})

                console.log(this.state.status)
                    })

        console.log(data)
    }


    render()
    {
        return (
        <div id="form-div">
            <form onSubmit={this.handleSubmit} name="sample-form">
            <table>
                <tbody>
                   <tr>
                   <td><label>ID : </label></td>
                   <td><input type="text" name="id" value={this.state.id} onChange={this.formIDChange}/></td>
                   </tr>
                   <tr>
                      <td><label>Name : </label></td>
                      <td><input type="text" name="name" value={this.state.name} onChange={this.formNameChange}/></td>
                  </tr>
                   <tr align="center">
                       <td colSpan="2"><input type="submit" value="Insert"/></td>
                   </tr>
                   </tbody>
               </table>
            </form>
        </div>
        )
    }
}

export default InsertEmp;
//ReactDOM.render(<DBForm />,document.querySelector('#react-form'));
