import React from 'react';
//import ReactDOM from 'react-dom';
import Employees from './Employees.js';
import InsertEmp from './InsertEmp.js';
import DeleteEmp from './DeleteEmp.js';

class DBOperation extends React.Component
{
    state ={
            selectedOption : ''
            }
     radioChange = this.radioChange.bind(this);

    /*constructor(props){
        super(props);
        this.state = {
            selectedOption : ''
        };
        this.radioChange = this.radioChange.bind(this);
    }*/

    radioChange(event){
        this.setState({selectedOption : event.target.value})
    }

    renderSwitch(param){
    const divStyle = {
      margin: '50px'
//      border: '5px solid pink'
    };



        switch(param){
            case 'GET':
            return (
                <div style={divStyle}>
                 <Employees />
                 </div>
                );
            case 'INSERT':
                return (
                   <div style={divStyle}>
                    <InsertEmp />
                    </div>
                        );
            case 'UPDATE':
                return (
                    <div style={divStyle}>
                     <InsertEmp />
                     </div>
                    );
            case 'DELETE':
                return (
                       <div style={divStyle}>
                        <DeleteEmp />
                        </div>
                    );
            default:
        }
    }

    render(){
    let option = this.state.selectedOption;


    return (

        <div>
            <input type="radio" value="GET" checked={this.state.selectedOption === "GET"} onChange={this.radioChange}/> Get All Employees
            <input type="radio" value="INSERT" checked={this.state.selectedOption === "INSERT"} onChange={this.radioChange}/> Insert
            <input type="radio" value="UPDATE" checked={this.state.selectedOption === "UPDATE"} onChange={this.radioChange}/> Update
            <input type="radio" value="DELETE" checked={this.state.selectedOption === "DELETE"} onChange={this.radioChange}/> Delete

            {this.renderSwitch(option)}
        </div>
        )
    }
}

export default DBOperation;

//ReactDOM.render(<DBOperation />,document.querySelector('#react'))